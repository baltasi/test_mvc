<?php

function open_database_connection()
{
    $host = 'localhost'; // адрес сервера
    $database = 'artur'; // имя базы данных
    $user = 'root'; // имя пользователя
    $password = '123456'; // пароль

    $link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
    $link->set_charset("utf8");

    return $link;
}


function close_database_connection($link)
{
    mysqli_close($link);

}

function get_all_result()
{
    $link = open_database_connection();

    $task = $link->query("SELECT * FROM `result` ORDER BY `id` DESC");

    while ($row = mysqli_fetch_assoc($task)) {
        $result[] = $row;
    }

    close_database_connection($link);

    return $result;
}

function get_add_test()
{

    $link = open_database_connection();


    $data = $_POST;

    if (isset( $data['do_signup'] )) {

        $errors = array();


        if (trim( $data['vopros'] ) == '') {
            $errors[] = 'вопрос';
        }

        if (trim( $data['answer'] ) == '') {
            $errors[] = 'ответ';
        }


        if (empty( $errors )) {
            $sql = "INSERT INTO  `vopros` (vopros, answer) VALUES ( '" . $data['vopros'] . "','" . $data['answer'] . "')";
            $link->query( $sql );
            header( "Location: /add" );
            echo '<div style="color: red;">Добавлено</div><hr>';
        } else
            echo '<div style="color: red;">Ошибка: введите ' . array_shift( $errors ) . '</div><hr>';
    }

    close_database_connection($link);


    return $data;
}

function get_all_vopros()
{

    $link = open_database_connection();


    $data = $link->query("SELECT * FROM `vopros` ORDER BY RAND() LIMIT 10");

    while ($row = mysqli_fetch_assoc($data)) {
        $vopros[] = $row;
    }



    close_database_connection($link);


    return $vopros;
}

function get_result()
{

    $link = open_database_connection();


    $data = $_POST;

    if (isset( $data['do_signup'] )) {

        $errors = array();


        if (trim( $data['name'] ) == '') {
            $errors[] = 'Фамилия';
        }



        if (empty( $errors )) {


            $ot = 0;

            foreach ($_REQUEST['vopros'] as $key => $value) {

                $search = $link->query("SELECT * FROM `vopros` WHERE `id` = '".$key."' AND `answer` = '".$value."'");
                $search = $search->fetch_assoc();

                if(!empty($search)){
                    $ot++;
                }

            }

            $percent = round($ot * 10);

            $sql = "INSERT INTO `result` (`name`,`result`,`percent`, `data`) VALUES ( '".$_REQUEST['name']."', $ot , $percent, now())";
            $link->query( $sql );
            header( "Location: /result" );
            echo '<div style="color: red;">Добавлено</div><hr>';

        } else
            echo '<div style="color: red;">Ошибка: введите ' . array_shift( $errors ) . '</div><hr>';
    }

    close_database_connection($link);


    return $data;
}

function get_vopros()
{
    $link = open_database_connection();

    $vop = $link->query("SELECT * FROM `vopros` ORDER BY `id` DESC");

    while ($row = mysqli_fetch_assoc($vop)) {
        $vopros[] = $row;
    }

    close_database_connection($link);

    return $vopros;
}


