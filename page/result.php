<?php include 'header.php' ?>

<table class="table_dark">
  <tr>
    <th>Фамилия</th>
    <th>Балл</th>
    <th>Процент(%)</th>
    <th>Время:</th>

    </tr>
    <?php foreach ($result as $res): ?>
  <tr>
    <td><?= $res['name'] ?></td>
    <td><?= $res['result'] ?>/10</td>
    <td><?= $res['percent'] ?>%</td>
    <td><?= $res['data'] ?></td>

    </tr>
    <?php endforeach; ?>
  </table>

<?php include 'footer.php' ?>
