<?php

// Загружаем и инициализируем глобальные библиотеки
require_once 'model.php';

require_once 'controllers.php';

// Внутренняя маршрутизация
http://$_SERVER['SERVER_NAME']$_SERVER['REQUEST_URI'];

$uri = $_SERVER['REQUEST_URI'];

if ($uri == '/') {
    show_index();
}elseif ($uri == '/result') {
    show_result();
}elseif ($uri == '/add') {
    show_add();
}elseif ($uri == '/test') {
    show_test();
}elseif ($uri == '/vopros') {
    show_vopros();
}else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Page Not Found</h1></body></html>';
}